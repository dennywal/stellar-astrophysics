This was a homework assignment I did for a class I took in stellar astrophysics.

In this homework assignment I analyzed the amount of mass enclosed in the sun as a function of it's radius as well as the amount of light it produces at different radii.
I also performed an analysis of the main sequence stars and observed that their masses fall below that of the Chandrasekar mass limit.
Contained here are also some images produced by the python notebook.

This assignment uses an ipython (jupyter) notebook as well as the scipy, numpy and matplotlib python libraries.