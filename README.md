# README #

This is a collection of homework assignments I did for a class I took in stellar astrophysics. 

Most of these python scripts require the numpy, scipy and matplotlib libraries.

Homework#7 also requires the use of an ipython notebook to view.
These python scripts deal with analysis and simulation of stars using both observed and simulated data.

Homework#4 contains a polytrope model for the sun and associated graphs.

Homework#5 contains the mass luminosity relationship for main sequence stars.

Homework#7 contains information on the mass distribution properties of the sun along with information on the masses of white dwarfs and the rotational velocities of main sequence 
stars of various different types.