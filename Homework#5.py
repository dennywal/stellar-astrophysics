from __future__ import division
import numpy as np
from matplotlib import pyplot as plt

main_sequence = open('MainSequence.txt')

lines = 48
for i in range(3):
    main_sequence.readline()

#print main_sequence.read()

M_array = np.zeros(lines)
L_array = np.zeros(lines)

for i in range(lines):
    main_sequence.read(21)

    M_array[i] = float(main_sequence.read(7))
    main_sequence.read(25)
    L_array[i] = float(main_sequence.read(8))

    main_sequence.readline()

L_array
print M_array
print L_array
slope = np.zeros(lines)
slope_1 = np.zeros(lines)
slope_3 = np.zeros(lines)
slope_5 = np.zeros(lines)
for i in range(lines):
    slope_1[i] = i/(30) - 0.2
    slope[i] = i/(30) - 0.17
    slope_3[i] = 3*i/(30) - 0
    slope_5[i] = 5.46*i/(30) - 1

plt.figure(1)
plt.title('Mass Luminosity relationship')
p1 = plt.plot(np.log10(M_array), L_array, 'g.')
p2 = plt.plot(slope_1, slope_3, 'r:')
p3 = plt.plot(slope, slope_5, 'b--')
plt.xlabel('log(Stellar Mass)')
plt.ylabel('log(Stellar Luminosity)')
plt.show()
