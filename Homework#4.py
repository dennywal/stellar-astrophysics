from __future__ import division
import numpy as np
from matplotlib import pyplot as plt

solar_model = open('SolarModel.txt')

lane_emden = open('Lane_emden.txt')

#Reading out the non-data lines from our files
for i in range(13):
    solar_model.readline()

for i in range(2):
    lane_emden.readline()

#Constants
lines = 127
lines_2 = 69
n = 3
k = 1.38e-16
m_H = 1.66e-24
R = 8.3145e7
G = 6.67e-8
mew_sol = 0.61
sigma = 5.67e-5
c = 3e10
a = (4/c)*sigma
T_eff = 5777
L_sol = 3.85e33
R_sol = 6.96e10
M_sol = 1.9891e33
Beta_sol = 0.9995853
D_n = 54.18
const = 4.3e25
rho_c = (D_n*M_sol)/(((4*np.pi)/3)*(R_sol**3))
K = ((3*(R**4)*(1-Beta_sol)/(a*(mew_sol**4)*(Beta_sol**4)))**(1/3))
alpha = (((n+1)*K)/(4*np.pi*G*(rho_c**((n-1)/n))))**(1/2)
print rho_c


#Defining arrays for given data
mass_array = np.zeros(lines)
radius_array = np.zeros(lines)
temp_array = np.zeros(lines)
density_array = np.zeros(lines)
luminosity_array = np.zeros(lines)
H_array = np.zeros(lines)
He_array = np.zeros(lines)
xi = np.zeros(lines_2)
theta = np.zeros(lines_2)

#print lane_emden.read()
for i in range(lines_2):
    #lane_emden.read(5)
    xi[i] = float(lane_emden.read(9))
    lane_emden.read(3)
    theta[i] = float(lane_emden.read(10))
    lane_emden.readline()
    
    

poly_radius_array = (alpha/R_sol)*xi



#Reading the data file into corresponding arrays
for i in range(lines):
    first = float(solar_model.read(6))
    solar_model.read(1)
    second = float(solar_model.read(3))
    mass = first * 10**second
    mass_array[i] = mass

    first = float(solar_model.read(7))
    solar_model.read(1)
    second = float(solar_model.read(3))
    radius = first * 10**second
    radius_array[i] = radius

    first = float(solar_model.read(7))
    solar_model.read(1)
    second = float(solar_model.read(3))
    temp = first * 10**second
    temp_array[i] = temp

    first = float(solar_model.read(7))
    solar_model.read(1)
    second = float(solar_model.read(3))
    rho = first * 10**second
    density_array[i] = rho

    first = float(solar_model.read(7))
    solar_model.read(1)
    second = float(solar_model.read(3))
    luminosity = first * 10**second
    luminosity_array[i] = luminosity

    first = float(solar_model.read(7))
    solar_model.read(1)
    second = float(solar_model.read(3))
    H = first * 10**second
    H_array[i] = H

    first = float(solar_model.read(7))
    solar_model.read(1)
    second = float(solar_model.read(3))
    He = first * 10**second
    He_array[i] = He

    solar_model.readline()

solar_model.close()
mew_inv_I = np.zeros(lines)
mew_inv_e = np.zeros(lines)
mew = np.zeros(lines)
Z = np.zeros(lines)

for i in range(lines):
    mew_inv_I[i] = H_array[i] + (He_array[i]/4) + \
                 (1 - H_array[i] - He_array[i])/20
    mew_inv_e[i] = (1/2) * (1 + H_array[i])
    Z[i] = 1 - H_array[i] - He_array[i]
    mew[i] = 1/(mew_inv_I[i] + mew_inv_e[i])



#Defining arrays for calulated data
Flux = np.zeros(lines)
P_rad = np.zeros(lines)
P_ion = np.zeros(lines)
P_e = np.zeros(lines)
P_gas = np.zeros(lines)
BB_flux = np.zeros(lines)
opacity = np.zeros(lines)
beta = np.zeros(lines)
P_tot = np.zeros(lines)
poly_rho = np.zeros(lines_2)
poly_P = np.zeros(lines_2)
dP_rad = np.zeros(lines)
dr = np.zeros(lines)
dP_dr = np.zeros(lines)
dP_dr_1 = np.zeros(lines)
dT = np.zeros(lines)
F_grav = np.zeros(lines)


for i in range(lines_2):
    poly_rho[i] = rho_c*(theta[i]**n)
    poly_P[i] = K*poly_rho[i]**(4/3)
    

#Calculating data arrays
for i in range(lines):
    
    P_rad[i] = (1/3)*a*(temp_array[i]**4)
    P_ion[i] = (mew_inv_I[i] * (k / m_H) * density_array[i] * temp_array[i])
    P_e[i] = (mew_inv_e[i] * (k / m_H) * density_array[i] * temp_array[i])
    P_gas[i] = (R/mew[i])*density_array[i]*temp_array[i]
    Flux[i] = L_sol*(luminosity_array[i]/((4*np.pi*(R_sol * radius_array[i])**2)))
    BB_flux[i] = sigma * (T_eff**4)
    opacity[i] = const*Z[i]*(1 + H_array[i])*density_array[i]/(temp_array[i]**3.5)
    P_tot[i] = P_rad[i] + P_ion[i] + P_e[i]
    beta[i] = -P_rad[i]/P_tot[i] + 1


for i in range(lines):
    #dT[i] = temp_array[i+1] - temp_array[i]
    #dP_rad[i] = P_rad[i+1]-P_rad[i]
    #dr[i] = radius_array[i+1]-radius_array[i]
    #dP_dr_1[i] = (temp_array[i]**3)*(dT[i]/dr[i])
    dP_dr[i] = -Flux[i]*opacity[i]*density_array[i]/c
    F_grav[i] = G*mass_array[i]*M_sol*density_array[i]/(radius_array[i]**2)

print temp_array
print np.log10(temp_array)
#Plotting the graphs of different variables as a function of radius
plt.figure(1)
plt.title('Pressure contributions vs radius')
p1 = plt.plot(radius_array, np.log10(P_rad), 'g')
p2 = plt.plot(radius_array, np.log10(P_ion), 'b')
p3 = plt.plot(radius_array, np.log10(P_e), 'r')
p4 = plt.plot(radius_array, np.log10(P_gas), 'y:')
plt.xlabel('Stellar Radius (Solar Radii)')
plt.ylabel('log(Pressure(ba))')
plt.legend((p1[0], p2[0], p3[0], p4[0]), ('rad', 'ion', 'elec', 'gas'))


plt.figure(2)
plt.title('Flux as a function of radius')
plt.xlabel('Stellar Radius (Solar Radii)')
plt.ylabel('log(Flux (ergs/s*cm^2)')
plt.plot(radius_array, np.log10(Flux), 'g')
plt.plot(radius_array, np.log10(BB_flux), 'b')

plt.figure(3)
plt.title('Opacity as a function of radius')
plt.xlabel('Stellar Radius (Solar Radii)')
plt.ylabel('log(opacity) (cm^2/g) ')
plt.plot(radius_array, np.log10(opacity), 'g')

plt.figure(4)
plt.title('Beta as a function of radius')
plt.xlabel('Stellar Radius (Solar Radii)')
plt.ylabel('Beta')
plt.plot(radius_array, beta)
#plt.plot(radius_array, mew, 'g--')

plt.figure(5)
plt.title('Density vs Radius')
plt.xlabel('Stellar Radius')
plt.ylabel('Density')
plt.plot(radius_array, np.log10(density_array))

plt.figure(6)
plt.title('Polytropic Pressure vs Radius')
plt.xlabel('Stellar Radius')
plt.ylabel('Pressure')
p8 = plt.plot(radius_array, (P_tot), 'g:')
p9 = plt.plot(poly_radius_array, (poly_P), 'b--')
plt.legend((p8[0], p9[0]), ('solar model', 'polytrope'))

plt.figure(7)
plt.title('Polytropic density vs Radius')
plt.xlabel('Stellar Radius')
plt.ylabel('Density')
p10 = plt.plot(radius_array, (density_array), 'g:')
p11 = plt.plot(poly_radius_array, (poly_rho), 'b--')
plt.legend((p10[0], p11[0]), ('solar model', 'polytrope'))

plt.figure(8)
plt.title('Opacity vs temp')
plt.xlabel('Temp')
plt.ylabel('k')
plt.plot(np.log10(temp_array), np.log10(opacity))

plt.figure(9)
plt.title('dP/dr vs radius')
plt.xlabel('radiuis (Solar Radii)')
plt.ylabel('dP/dr')
p12 = plt.plot(radius_array, np.log10(np.abs(dP_dr)), 'g')
p13 = plt.plot(radius_array, np.log10(F_grav), 'b')
plt.legend((p12[0], p13[0]), ('dP/dr', 'F_grav'))

plt.show()
